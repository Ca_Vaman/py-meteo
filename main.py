from meteofrance_api import MeteoFranceClient
# from meteofrance_api.helpers import readeable_phenomenoms_dict
import renderer
from settings import settings


if __name__ == "__main__":
    client = MeteoFranceClient()
    list_places = client.search_places(settings.CITY)

    # TODO : Filter place by country
    my_place = list_places[0]

    my_place_weather_forecast = client.get_forecast_for_place(my_place)
    my_place_daily_forecast = my_place_weather_forecast.daily_forecast

    today_forecast = renderer.rendering_daily_forecast(my_place_daily_forecast[0])

