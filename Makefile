remove:
	rm -rf venv/

install: remove
	python3 -m venv venv


clear:
	rm -f *.pyc
	rm -rf */__pycache__
	rm -fr __pycache__
