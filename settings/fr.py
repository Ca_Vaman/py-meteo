MINIMAL_TODAY = "Température minimale prévue : {}°C"
MAXIMAL_TODAY = "Température maximale prévue : {}°C"
NO_RAIN_TODAY = "Pas de pluie prévue"
RAIN_TODAY = "De la pluie est prévue"
TODAY_DATE_REPORT = "Rapport météo pour la journée du {} {} {} {}"
CITY_COUNTRY = "Ville de {}, {}"
WEATHER = "Temps prévu: {}"

TRANSLATED_DAYS = {
    'Sun': 'Dimanche',
    'Mon': 'Lundi',
    'Tue': 'Mardi',
    'Wed': 'Mercredi',
    'Thu': 'Jeudi',
    'Fri': 'Vendredi',
}

TRANSLATED_MONTH = {
    'Jan': 'Janvier',
    'Feb': 'Février',
    'Mar': 'Mars',
    'Apr': 'Avril',
    'May': 'Mai',
    'Jun': 'Juin',
    'Jul': 'Juillet',
    'Aug': 'Août',
    'Sep': 'Septembre',
    'Oct': 'Octobre',
    'Nov': 'Novembre',
    'Dec': 'Décembre',
}
