MINIMAL_TODAY = "Minimal temperature : {}°C"
MAXIMAL_TODAY = "Maximale temperature : {}°C"
NO_RAIN_TODAY = "No rain for today"
RAIN_TODAY = "Might expect some rain today"
TODAY_DATE_REPORT = "Weather forecast for {} {} {} {}"
CITY_COUNTRY = "City of {}, {}"
WEATHER = "Climate: {}"

TRANSLATED_DAYS = {
    'Sun': 'Sunday',
    'Mon': 'Monday',
    'Tue': 'Tuesday',
    'Wed': 'Wednesday',
    'Thu': 'Thursday',
    'Fri': 'Friday',
}

TRANSLATED_MONTH = {
    'Jan': 'January',
    'Feb': 'February',
    'Mar': 'March',
    'Apr': 'April',
    'May': 'May',
    'Jun': 'June',
    'Jul': 'July',
    'Aug': 'August',
    'Sep': 'September',
    'Oct': 'October',
    'Nov': 'November',
    'Dec': 'December',
}


