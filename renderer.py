import time
from settings import settings


if settings.LANGUAGE == "FR":
    import settings.fr as translations
else:
    import settings.default_translation as translations


def rendering_daily_forecast(forecast):
    print(forecast)

    print(
        """
{}
{}
========
{}
{}
{}

{}
        """.format(
            rendering_today_date(forecast),
            rendering_city(),
            rendering_weather(forecast),
            rendering_minimal(forecast),
            rendering_maximal(forecast),
            rendering_rain_forecast(forecast)
        )
          )


def rendering_minimal(forecast):
    return translations.MINIMAL_TODAY.format(forecast['T']['min'])


def rendering_maximal(forecast):
    return translations.MAXIMAL_TODAY.format(forecast['T']['max'])


def rendering_rain_forecast(forecast):
    if forecast['precipitation']['24h'] > 0:
        return translations.RAIN_TODAY
    else:
        return translations.NO_RAIN_TODAY


def rendering_today_date(forecast):
    time_formatted = time.strftime('%a %d %b %Y', time.localtime(forecast['dt']))
    date_list = time_formatted.split()

    return translations.TODAY_DATE_REPORT.format(
        translations.TRANSLATED_DAYS[date_list[0]],
        date_list[1],
        translations.TRANSLATED_MONTH[date_list[2]],
        date_list[3],
    )


def rendering_city():
    return translations.CITY_COUNTRY.format(settings.CITY, settings.COUNTRY)


def rendering_weather(forecast):
    return translations.WEATHER.format(forecast['weather12H']['desc'])
